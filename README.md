# vue-auto-route-cli

## Getting started

> npm i -g genertor-router  
> gen -V

## usage

### Windows 初始化项目

> gen init 'name'

### 目录结构说明

    |-- .babelrc            babel配置文件
    |-- .gitignore          git忽略文件
    |-- babel.config.js     babel配置
    |-- package.json        项目相关依赖
    |-- README.md           readme文件
    |-- tsconfig.json       ts全局配置文件
    |-- yarn.lock
    |-- public              公共资源文件
    |   |-- favicon.ico
    |   |-- index.html
    |   |-- robots.txt
    |   |-- img
    |       |-- icons
    |           |-- android-chrome-192x192.png
    |           |-- android-chrome-512x512.png
    |           |-- android-chrome-maskable-192x192.png
    |           |-- android-chrome-maskable-512x512.png
    |           |-- apple-touch-icon-120x120.png
    |           |-- apple-touch-icon-152x152.png
    |           |-- apple-touch-icon-180x180.png
    |           |-- apple-touch-icon-60x60.png
    |           |-- apple-touch-icon-76x76.png
    |           |-- apple-touch-icon.png
    |           |-- favicon-16x16.png
    |           |-- favicon-32x32.png
    |           |-- msapplication-icon-144x144.png
    |           |-- mstile-150x150.png
    |           |-- safari-pinned-tab.svg
    |-- src                               业务相关文件
    |   |-- App.vue
    |   |-- main.ts                       入口文件
    |   |-- registerServiceWorker.ts      serviceWork文件配置
    |   |-- shims-vue.d.ts                vue-ts生明文件
    |   |-- api                           api配置文件
    |   |   |-- index.ts
    |   |-- assets                        静态资源文件
    |   |   |-- logo.png
    |   |-- components                    公用组件
    |   |   |-- HelloWorld.vue
    |   |   |-- NavBar.vue
    |   |   |-- Progress.vue
    |   |   |-- Test.vue
    |   |-- interface                     ts定义接口文件
    |   |   |-- index.ts
    |   |-- router                        路由文件
    |   |   |-- index.ts
    |   |-- store                         vuex文件
    |   |   |-- index.ts
    |   |-- utility                       公用方法文件
    |   |   |-- index.ts                  页面文件
    |   |-- views
    |       |-- Create.vue
    |       |-- Home.vue
    |       |-- ShoppingCart.vue
    |-- template                          模板文件
        |-- nav.vue.hbs
        |-- router.ts.hbs

### refresh （生成路由配置）

> gen refresh  
> name为view文件夹下的文件名 做了一次转小写操作
```javascript
  //生成的路由
  {
    path:`/${name}`,
    name:`${name}`,
    component:()=>import(`@/views/${file}`)
  }
```

```html
<!-- 生成的导航 -->
<div class="router-link">
  <router-link to="/{{name}}">{{name}}</router-link>
</div>
```
