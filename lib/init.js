const { promisify } = require('util');
const figlet = promisify(require('figlet'));
const clear = require('clear');
const chalk = require('chalk');
const { clone } = require('./dowonload');
const log = content => console.log(chalk.green(content));

const spawn = async (...args) => {
  const { spawn } = require('child_process');
  return new Promise((res, rej) => {
    try {
      const proc = spawn(...args);
      proc.stdout.pipe(process.stdout);
      proc.stderr.pipe(process.stderr);
      proc.on('close', () => {
        res();
      });
    } catch (error) {
      rej(error);
    }
  });
};

module.exports = async name => {
  // 打印欢迎界面
  clear();
  const data = await figlet('kkb welcome');
  log(data);
  log(`🚀创建项目: ${name}`);
  // 初始化
  await clone('gitlab:jeremy-liucheng/vue-admin#main', name);
  log('下载依赖');
  try {
    await spawn('yarn.cmd', ['install'], { cwd: `./${name}` });
    log(`👌安装完成:
      To get Starts:
      ========================
      cd ${name}
      npm run serve
      ========================
                             `);
    const open = require('open');
    open('http://localhost:8080');
    await spawn('yarn.cmd', ['serve'], { cwd: `./${name}` });
  } catch (error) {
    log(`o(╥﹏╥)o安装失败:${error}`);
    fs.unlinkSync(`${__dirname}/${name}`);
  }
};
