const fs = require('fs');
const handlebars = require('handlebars');
const chalk = require('chalk');
const log = content => console.log(chalk.green(content));
module.exports = async () => {
  //获取页面列表
  const list = fs
    .readdirSync(`./src/views`)
    .filter(v => v !== 'Home.vue')
    .map(v => ({
      name: v.replace('.vue', '').toLowerCase(),
      file: v,
    }));
  //路由
  compile({ list }, './src/router/index.ts', './template/router.ts.hbs');
  //菜单
  compile({ list }, './src/components/NavBar.vue', './template/nav.vue.hbs');
  /**
   *
   *
   * @param {*} meta 数据定义
   * @param {*} filePath 目标文件
   * @param {*} templatePath 模板文件
   */
  function compile(meta, filePath, templatePath) {
    if (fs.existsSync(templatePath)) {
      const content = fs.readFileSync(templatePath).toString();
      const result = handlebars.compile(content)(meta);
      fs.writeFileSync(filePath, result);
      log(`🚀${filePath}创建成功`);
    }
  }
};
